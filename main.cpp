#include <iostream>
#include <X11/X.h>
#include <X11/Xlib.h>
//#include <X11/Xlibint.h>
#include <X11/Xutil.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <X11/extensions/XShm.h>
#include <thread>
#include <chrono>
#include <boost/program_options.hpp>
#include <fstream>
#include <csignal>

bool exit_flag;

void signal_handler(int signal_number) {
    if (signal_number == 2)
        exit_flag = true;
}

short get_brightness(float r, float g, float b, short lowest_brightness, short highest_brightness) {
    float max = 0;
    if (r >= g) {
        max = r;
        if (r < b)
            max = b;
    } else {
        max = g;
        if (g < b)
            max = b;
    }
    return (short)(max / 255 * (highest_brightness - lowest_brightness) + lowest_brightness);
}

int main() {
    std::signal(SIGINT, signal_handler);
    exit_flag = false;
    namespace po = boost::program_options;
    std::string brightness_device;
    short lowest_brightness = 1;
    short highest_brightness = 100;
    short probes_percent = 100;
    short probes_per_second = 1;
    short current_brightness = 100;

    // Setup options.
    po::options_description desc("settings");
    desc.add_options()
            ("brightness_device", po::value<std::string>(&brightness_device)->multitoken(), "brightness file/device")
            ("lowest_brightness", po::value<short>(&lowest_brightness)->multitoken(), "lowest brightness")
            ("highest_brightness", po::value<short>(&highest_brightness)->multitoken(), "highest brightness")
            ("probes_percent", po::value<short>(&probes_percent)->multitoken(), "percent of probes")
            ("probes_per_second", po::value<short>(&probes_per_second)->multitoken(), "brightness change speed");

    // Load setting file.
    po::variables_map vm;
    std::ifstream settings_file(".settings", std::ifstream::in);
    po::store(po::parse_config_file(settings_file, desc), vm);
    settings_file.close();
    po::notify(vm);

    std::ofstream test_file(brightness_device, std::ofstream::out);
    if (!test_file.is_open()) {
        std::cerr << "file for brightness device not found" << std::endl;
        return 1;
    }
    test_file.close();

    XShmSegmentInfo shminfo;
    Display *dpy = XOpenDisplay(nullptr);
    if (dpy == nullptr) {
        std::cerr << "Cannot open display" << std::endl;
        return 1;
    }

    unsigned int width, height;
    int scr = XDefaultScreen(dpy);
    width = DisplayWidth(dpy, scr);
    height = DisplayHeight(dpy, scr);
    Window root = RootWindow(dpy, scr);

    XImage *image = XShmCreateImage(dpy, DefaultVisual(dpy, scr), DefaultDepth(dpy, scr), ZPixmap, nullptr, &shminfo, width, height);

    shminfo.shmid = shmget(IPC_PRIVATE, image->bytes_per_line * image->height, IPC_CREAT | 0777);
    if (shminfo.shmid == -1) {
        std::cerr << "Cannot get shared memory" << std::endl;
        return 1;
    }
    shminfo.shmaddr = image->data = (char *)shmat(shminfo.shmid, 0, 0);
    shminfo.readOnly = False;
    if (!XShmAttach(dpy, &shminfo)) {
        std::cerr << "Failed to attach shared memory" << std::endl;
        return 1;
    }

    float x_add = (float)width / (probes_percent / 100.0f * width), y_add = (float)height / (probes_percent / 100.0f * height);

    while (!exit_flag) {
        XShmGetImage(dpy, root, image, 0, 0, AllPlanes);

        XSync(dpy, True);

        unsigned long r = 0, g = 0, b = 0;
        unsigned long count = 0;

        for (float x = 0; x < width; x += x_add) {
            for (float y = 0; y < height; y += y_add, ++count) {
                unsigned long pixel = XGetPixel(image, (int)x, (int)y);
                r += (pixel >> 16) & 0xff;
                g += (pixel >> 8) & 0xff;
                b += (pixel >> 0) & 0xff;
            }
        }
        short ideal_brightness = get_brightness((float)r / count, (float)g / count, (float)b / count, lowest_brightness, highest_brightness);
        if (current_brightness > ideal_brightness)
            current_brightness--;
        else if (current_brightness < ideal_brightness)
            current_brightness++;
        std::ofstream brightness_file(brightness_device, std::ofstream::out);
        if (!brightness_file.is_open()) {
            std::cerr << "file for brightness device not found" << std::endl;
            return 1;
        }
        brightness_file << current_brightness;
        brightness_file.close();
        std::this_thread::sleep_for(std::chrono::milliseconds(1000/probes_per_second));
    }

    XShmDetach(dpy, &shminfo);
    shmdt(shminfo.shmaddr);
    shmctl(shminfo.shmid, IPC_RMID, nullptr);
    XDestroyImage(image);
    image = nullptr;
    XCloseDisplay(dpy);
    std::cout << "Thank you for waiting!" << std::endl;
    return 0;
}
